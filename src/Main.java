import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Generic<Integer> gen =new Generic<>();
        List<Integer> listWrapper = new ArrayList<>();
        //Add Value
        listWrapper.add(0);
        listWrapper.add(11);
        listWrapper.add(null);
        listWrapper.add(1);
        listWrapper.add(50);
        listWrapper.add(80);
        listWrapper.add(null);
        listWrapper.add(100);
        listWrapper.add(20);

        gen.addItem(listWrapper);

        //Display
        listWrapper = gen.getItem();
        for(int i=0;i<listWrapper.size();i++){
            System.out.println(listWrapper.get(i));
        }
        System.out.println("------------------------------");
    }
}

